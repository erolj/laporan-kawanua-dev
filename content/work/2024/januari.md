+++
title = 'Januari'
date = 2024-03-06T00:05:00+08:00
publishdate = 2024-03-06T00:05:00+08:00
description = "Develop draft landing page halaman profil Sulawesi Utara."
icon = "calendar_month"
author = "Erol Tooy"
draft = false
toc = true
weight = 100
tags = ["Beginners"]
+++

## Landing Page Profil Sulut

{{< table "table-responsive" >}}
| Tanggal | Deskripsi |
|---|---|
| 22-31 Januari | (1) Membuat static page berbasis HTML5 + SCSS + BS 5, untuk landing page Profil Sulawesi Utara. (2) Mengunggah source-code ke git repo. (3) Melakukan deploy dari git repo ke platform serverless. |
| | Demo URL: https://sulut.erto.work |
| | Git Repo: https://git.sulutprov.go.id/erol/profilsulut-draft.git |

<!-- ![Landing page](/assets/images/januari-01.png "Gambar 1 Landing page") -->

{{< /table >}}
